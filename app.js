const express = require("express")
const path = require("path")
const logger = require("morgan")
const cors = require("cors")

const apiRouter = require("./routes/api")

const app = express()

app.use(logger("dev"))
app.use(express.json())

app.use(cors())

app.use(express.static(path.join(__dirname, "webapp/build")))
app.use("/api", apiRouter)

app.get("*", (req,res) => {
  res.sendFile(path.join(__dirname, "webapp/build/index.html"))
})



module.exports = app
