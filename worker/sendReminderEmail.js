const sgMail = require("@sendgrid/mail")
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const FROM_ADDRESS = process.env.SENDGRID_FROM_ADDRESS

const buildEmailText = (user, notes) => {
  const notesString = notes.map((n, i) => `${i+1}. ${n.content}`).join("\n")
  return `Hi ${user.email},

Here is your daily email with all your notes:

${notesString}

Thank you for using our service!
`
}

const sendEmail = async (user, text) => {
  const message = {
    to: user.email,
    from: FROM_ADDRESS,
    subject: "Daily reminder of your notes",
    text: text
  }

  try {
    const result = await sgMail.send(message)
    console.log(`Successfully sent email for user ${user.email}:`, result)
  } catch(e) {
    console.error(`Failed to send email for user ${user.email}...`, JSON.stringify(e))
  }
}

const sendReminderEmail = async (user, notes) => {
  if (!notes.length) return console.log(`skipping user ${user.email} because they have 0 notes`)

  const text = buildEmailText(user, notes)
  console.log("Text:", text)
  await sendEmail(user, text)
}

module.exports = sendReminderEmail