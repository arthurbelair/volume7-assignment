require("dotenv").config()

const sendReminderEmail = require("./sendReminderEmail")

const User = require("../models").User
const Note = require("../models").Note

const processUser = async (user) => {
  const notes = await Note.findAll({ where: { userId: user.id } })

  console.log(`Found ${notes.length} notes for user ${user.id}`)
  sendReminderEmail(user, notes)
}

const startJob = async () => {
  const users = await User.findAll({})

  console.log(`found ${users.length} users`)
  for (let u of users) {
    await processUser(u)
  }
}

startJob()