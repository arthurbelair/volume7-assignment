"use strict"
const { Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {
  class Note extends Model {
    static associate(models) {
      Note.belongsTo(models.User, { foreignKey: "userId", foreignKeyConstraint: { allowNull: false } })
    }
  }
  Note.init({
    content: DataTypes.STRING
  }, {
    sequelize,
    modelName: "Note",
  })
  return Note
}