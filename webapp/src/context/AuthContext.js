import React from "react"

const DEFAULT = {
  token: null
}

const AuthContext = React.createContext(DEFAULT)

export default AuthContext