import React, { useCallback, useEffect, useMemo, useState } from "react"
import PropTypes from "prop-types"
import { decode } from "jsonwebtoken"
import AuthContext from "../AuthContext"

const validateToken = (token) => {
  const decoded = decode(token)
  if (!decoded) return null
  if (decoded.exp*1000 < Date.now()) return null
  return decoded
}

const TOKEN_KEY = "auth_token"

const getTokenFromLocalStorage = () => {
  const existingToken = localStorage.getItem(TOKEN_KEY)
  if (validateToken(existingToken)) return existingToken
  else { localStorage.removeItem(TOKEN_KEY) }
  return null
}

const AuthContextProvider = props => {

  const [tokenState, setTokenState] = useState(getTokenFromLocalStorage())

  const clearToken = useCallback(() => {
    setTokenState(null)
    localStorage.removeItem(TOKEN_KEY)
  }, [])

  const setToken = useCallback((token) => {
    if (!validateToken(token)) clearToken()
    localStorage.setItem(TOKEN_KEY, token)
    setTokenState(token)
  }, [clearToken])

  useEffect(() => {
    if (tokenState) {
      if (!validateToken(tokenState)) clearToken()
    }
  }, [tokenState, clearToken])

  const logout = useCallback(() => {
    clearToken()
  }, [clearToken])

  const userInfo = useMemo(() => {
    if (tokenState && tokenState.length) {
      return decode(tokenState)
    } else {
      return {}
    }
  }, [tokenState])

  const context = useMemo(() => {
    return {
      token: tokenState,
      setToken,
      userInfo,
      logout
    } 
  }, [tokenState, setToken, userInfo, logout])

  return (
    <AuthContext.Provider value={context}>
      {props.children}
    </AuthContext.Provider>
  )
}

AuthContextProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired
}

export default AuthContextProvider