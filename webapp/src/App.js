import React from "react"
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"
import AuthContextProvider from "./context/providers/AuthContextProvider"
import Dashboard from "./components/pages/Dashboard"
import Login from "./components/pages/Login"
import SignUp from "./components/pages/SignUp"

function App() {
  return (
    <div className="App">
      <AuthContextProvider>
        <BrowserRouter>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route path="/" render={() => {
              return <Redirect to="/dashboard" />
            }} />
          </Switch>
        </BrowserRouter>
      </AuthContextProvider>
    </div>
  )
}

export default App
