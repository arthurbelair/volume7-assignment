import { useContext, useMemo } from "react"
import AuthContext from "../context/AuthContext"

const useAuthContext = () => {
  const {
    token,
    clearToken,
    setToken,
    userInfo,
    logout
  } = useContext(AuthContext)

  const isLoggedIn = useMemo(() => {
    return !!token
  }, [token])
  
  return {
    token,
    clearToken,
    setToken,
    isLoggedIn,
    userInfo,
    logout
  }
}

useAuthContext.propTypes = {
  
}

export default useAuthContext