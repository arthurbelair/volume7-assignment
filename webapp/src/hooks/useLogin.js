import { useCallback } from "react"
import PropTypes from "prop-types"
import { usePost } from "./useAPI"
import useAuthContext from "./useAuthContext"

const useLogin = () => {
  const authContext = useAuthContext()
  const { execute, loading, error } = usePost("/login")

  const login = useCallback(async ({ email, password }) => {
    const result = await execute({ email, password })
    if (result) {
      const { token } = result
      authContext.setToken(token)
    }
  }, [execute, authContext])

  return {
    login,
    loading,
    error
  }
}

useLogin.propTypes = {
  onLoggedIn: PropTypes.func,
}

export default useLogin