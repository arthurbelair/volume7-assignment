import axios from "axios"
import { useCallback, useMemo, useState } from "react"
import useAuthContext from "./useAuthContext"

const API_URL = process.env.REACT_APP_API_URL
const buildURL = (path) => `${API_URL}${path}`

const handleError = (e) => {
  if (e.response) {
    try {
      const { error } = e.response.data
      return error
    } catch(e) {
      return "Unknown error."
    }
  } else {
    return e.message
  }
}

const useAPIHeaders = () => {
  const { token } = useAuthContext()

  const headers = useMemo(() => {
    if (token) return { "Authorization": token }
    return {}
  }, [token])

  return { headers }
}

const useRequest = (path, { method }) => {
  const { headers } = useAPIHeaders()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const [result, setResult] = useState(null)

  const execute = useCallback(async (data) => {
    const url = buildURL(path)
    setLoading(true)
    try {
      const response = await axios.request({ url, method, data, params: method === "GET" ? data : null, headers })
      setResult(response.data)
      setLoading(false)
      return response.data
    } catch(e) {
      const message = handleError(e)
      setError(message)
      setLoading(false)
      return null
    }

  }, [path, method, headers])

  return {
    execute,
    loading,
    error,
    result
  }
}

export const usePost = (path) => {
  return useRequest(path, { method: "POST" })
}

export const useGet = (path) => {
  return useRequest(path, { method: "GET" })
}

export const useDelete = (path) => {
  return useRequest(path, { method: "DELETE" })
}