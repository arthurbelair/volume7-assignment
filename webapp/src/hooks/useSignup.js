import { useCallback } from "react"
import PropTypes from "prop-types"
import { usePost } from "./useAPI"
import useAuthContext from "./useAuthContext"

const useSignup = () => {
  const authContext = useAuthContext()
  const { execute, loading, error } = usePost("/signup")

  const signup = useCallback(async ({ email, password }) => {
    const result = await execute({ email, password })
    if (result) {
      const { token } = result
      authContext.setToken(token)
    }
  }, [execute, authContext])

  return {
    signup,
    loading,
    error
  }
}

useSignup.propTypes = {
  onSignedUp: PropTypes.func,
}

export default useSignup