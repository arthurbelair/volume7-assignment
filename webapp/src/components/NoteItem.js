import React, { useCallback, useEffect } from "react"
import PropTypes from "prop-types"
import { Button, List, message, Typography } from "antd"
import { useDelete } from "../hooks/useAPI"

const NoteItem = ({ item, index, onDeleted }) => {

  const { execute, loading, error } = useDelete(`/notes/${item.id}`)

  const deleteNote = useCallback(async () => {
    await execute()
    onDeleted()
  }, [onDeleted, execute])

  useEffect(() => {
    if (error) message.error(error)
  }, [error])

  return (
    <List.Item>
      <Typography.Text>{index+1}. {item.content}</Typography.Text>
      <Button danger style={{float:"right"}} loading={loading} onClick={deleteNote}>Delete</Button>
    </List.Item>
  )
}

NoteItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.oneOf([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
    content: PropTypes.string.isRequired,
  }).isRequired,
  index: PropTypes.number.isRequired,
  onDeleted: PropTypes.func.isRequired,
}

export default NoteItem