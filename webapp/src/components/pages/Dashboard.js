import React from "react"
import useAuthContext from "../../hooks/useAuthContext"
import { Redirect } from "react-router-dom"
import { Button, Layout, PageHeader, Typography } from "antd"
import NoteList from "../NoteList"

const Dashboard = () => {

  const authContext = useAuthContext()

  if (!authContext.token) {
    return <Redirect to="/login"></Redirect>
  }

  const logout = () => {
    authContext.logout()
  }

  return (
    <Layout>
      <PageHeader>
        <Typography.Title level={3}>Welcome, {authContext.userInfo.email}<Button style={{float: "right"}} onClick={logout}>Log Out</Button></Typography.Title>
        
      </PageHeader>
      <Layout.Content style={{padding: "40px"}}>
        <NoteList />
      </Layout.Content>
    </Layout>
  )
}

Dashboard.propTypes = {
  
}

export default Dashboard