import React, { useEffect } from "react"
import { Button, Form, Input, Layout, message, PageHeader, Row, Typography } from "antd"
import { UserOutlined, LockOutlined } from "@ant-design/icons"
import {EMAIL_REGEX} from "../../utilities/regex"
import useLogin from "../../hooks/useLogin"
import useAuthContext from "../../hooks/useAuthContext"
import { Redirect } from "react-router-dom"


const Login = () => {
  const authContext = useAuthContext()

  const { login, loading, error } = useLogin()

  const onFinish = (values) => {
    const { email, password } = values
    login({ email, password })
  }

  useEffect(() => {
    if (error) message.error(error)
  }, [error])

  if (authContext.isLoggedIn) {
    return <Redirect to="/dashboard" /> 
  }

  return (
    <Layout>
      <PageHeader><Typography.Title style={{textAlign:"center"}}>Log In</Typography.Title></PageHeader>
      <Layout.Content>
        <Row type="flex" justify="center" style={{minHeight: "100vh"}}>
          <Form
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Form.Item
              name="email"
              validateTrigger="onBlur"
              rules={[
                { required: true, message: "Please enter your email." },
                { pattern: EMAIL_REGEX, message: "Please enter a valid email address." }
              ]}
            >
              <Input prefix={<UserOutlined />} type="email" placeholder="Email" />
            </Form.Item>
            <Form.Item
              name="password"
              validateTrigger="onBlur"
              rules={[
                { required: true, message: "Please input your password!" },
                { min: 6, message: "Password must be at least 6 characters." }
              ]}
            >
              <Input
                prefix={<LockOutlined />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading}>Log in</Button>
            </Form.Item>
            <Form.Item>
              <Button type="link" htmlType="button" href="/signup">Sign Up</Button>
            </Form.Item>
          </Form>
        </Row>
      </Layout.Content>
    </Layout>
    
  )
}


Login.propTypes = {
  
}

export default Login