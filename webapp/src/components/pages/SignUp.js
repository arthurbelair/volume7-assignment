import React, { useEffect } from "react"
import { Button, Form, Input, Layout, Row, message, PageHeader, Typography } from "antd"
import { UserOutlined, LockOutlined } from "@ant-design/icons"
import {EMAIL_REGEX} from "../../utilities/regex"
import useSignup from "../../hooks/useSignup"
import { Redirect } from "react-router-dom"
import useAuthContext from "../../hooks/useAuthContext"

const SignUp = () => {
  const authContext = useAuthContext()
  const { signup, loading, error } = useSignup()

  const onFinish = async (values) => {
    const { email, password } = values
    signup({ email, password })
  }

  useEffect(() => {
    if (error) message.error(error)
  }, [error])

  if (authContext.isLoggedIn) {
    return <Redirect to="/dashboard" /> 
  }

  return (
    <Layout>
      <PageHeader><Typography.Title style={{textAlign:"center"}}>Sign Up</Typography.Title></PageHeader>
      <Layout.Content>
        <Row type="flex" justify="center" style={{minHeight: "100vh"}}>
          <Form
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Form.Item
              name="email"
              validateTrigger="onBlur"
              rules={[
                { required: true, message: "Please enter your email." },
                { pattern: EMAIL_REGEX, message: "Please enter a valid email address." }
              ]}
            >
              <Input prefix={<UserOutlined />} type="email" placeholder="Email" />
            </Form.Item>
            <Form.Item
              name="password"
              validateTrigger="onBlur"
              rules={[
                { required: true, message: "Please input your password!" },
                { min: 6, message: "Password must be at least 6 characters." }
              ]}
            >
              <Input
                prefix={<LockOutlined />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading}>Sign Up</Button>
            </Form.Item>
            <Form.Item>
              <Button type="link" htmlType="button" href="/login">Log In</Button>
            </Form.Item>
          </Form>
        </Row>
      </Layout.Content>
    </Layout>
    
  )
}


SignUp.propTypes = {
  
}

export default SignUp