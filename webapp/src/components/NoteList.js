import React, { useEffect, useMemo } from "react"
// import PropTypes from "prop-types"
import { Input, List, message, Typography, Form, Button, Divider } from "antd"
import { useDelete, useGet, usePost } from "../hooks/useAPI"
import NoteItem from "./NoteItem"

const NoteList = () => {

  const [form] = Form.useForm()

  const { execute: fetchNotes, loading, error, result = {} } = useGet("/notes")

  const notes = useMemo(() => {
    if (result && result.notes) return result.notes
    return []
  }, [result])

  useEffect(() => {
    fetchNotes()
  }, [fetchNotes])

  const { execute: addNote, error: addError, loading: addLoading } = usePost("/notes")

  const onFinish = async (values) => {
    const { content } = values
    await addNote({ content })
    form.resetFields()
    fetchNotes()
  }

  useEffect(() => {
    if (error) message.error(error)
    if (addError) message.error(addError)
  }, [error, addError])

  return (
    <>
      <Typography.Title level={4}>Your Notes</Typography.Title>
      <Typography.Title level={5} type="secondary">They will be sent back to your email address every 24 hours.</Typography.Title>
      <List
        loading={loading}
        bordered
        dataSource={notes}
        renderItem={(item, index) => <NoteItem item={item} index={index} onDeleted={fetchNotes} />}
      />
      <Divider></Divider>
      <Form form={form} onFinish={onFinish} validateTrigger="onBlur">
        <Form.Item 
          name="content" 
          rules={[
            { max: 140, min: 1, required: true, message: "Note must be between 1 and 140 characters." }
          ]}
        >
          <Input disabled={addLoading} placeholder="My new note..." type="text" />
        </Form.Item>
        <Form.Item>
          <Button disabled={addLoading} type="primary" htmlType="submit" loading={addLoading}>Add</Button>
        </Form.Item>
      </Form>
    </>
  )
}

NoteList.propTypes = {
  
}

export default NoteList