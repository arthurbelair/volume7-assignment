const bcrypt = require("bcrypt")
const generateJWT = require("./lib/generateJWT")
const User = require("../models").User
const handleError = require("./lib/handleError")

module.exports = async (req, res) => {
  const { email, password } = req.body

  const user = await User.findOne({
    where: { email }
  })

  if (!user) return handleError(res, "Login failed. Please check your credentials.", 401)

  const passwordMatch = bcrypt.compareSync(password, user.password_hash)
  if (!passwordMatch) return handleError(res, "Login failed. Please check your credentials.", 401)

  const token = generateJWT(user, { issuer: "api/login" })

  res.json({ token })
}