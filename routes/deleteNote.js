const handleError = require("./lib/handleError")

const Note = require("../models").Note

module.exports = async (req, res) =>{
  const { noteId } = req.params
  const { userInfo } = res.locals

  try {
    const note = await Note.findOne({ where: { id: noteId, userId: userInfo.sub } })
    if (!note) return res.status(404).json({ error: "Could not find this note." })
    await note.destroy()
    res.status(204).end()
  } catch(e) {
    handleError(res, e, 500)
  }
}