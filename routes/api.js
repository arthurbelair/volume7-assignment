const { body, param } = require("express-validator")

const express = require("express")
const validateRequest = require("./middlewares/validateRequest")
const signup = require("./signup")
const login = require("./login")
const createNote = require("./createNote")
const router = express.Router()
const authenticate = require("./middlewares/authenticate")
const getNotes = require("./getNotes")
const deleteNote = require("./deleteNote")

/* GET home page. */
router.post("/signup", [
  body("email").isEmail().normalizeEmail().withMessage("Please enter a valid email address."),
  body("password").isString().isLength({ min: 6, max: 64 }).withMessage("Please enter a valid password."),
  validateRequest
], signup)

router.post("/login", [
  body("email").isEmail().normalizeEmail().withMessage("Please enter a valid email address."),
  body("password").isString().isLength({ min: 6, max: 64 }).withMessage("Please enter a valid password."),
  validateRequest
], login)

router.post("/notes", authenticate, [
  body("content").isString().isLength({ min: 1, max: 140 }).withMessage("Note cannot be empty and must be 140 characters max."),
  validateRequest,
], createNote)

router.get("/notes", authenticate, getNotes)

router.delete("/notes/:noteId", authenticate, [
  param("noteId").isNumeric(),
  validateRequest
], deleteNote)

module.exports = router
