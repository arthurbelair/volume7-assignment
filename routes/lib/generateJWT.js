const jwt = require("jsonwebtoken")
const { v4 } = require("uuid")

const JWT_SIGNING_KEY = process.env.JWT_SIGNING_KEY

const API_URI = `${process.env.HOST}:${process.env.PORT}`
const AUDIENCE = `${API_URI}/api/*`

const JWT_TTL = "30d"

module.exports = (user, { issuer = "_default" } = {}) => {
  const token_id = v4()

  const { id, email } = user

  return jwt.sign({
    email,
  }, JWT_SIGNING_KEY, {
    subject: id.toString(),
    expiresIn: JWT_TTL,
    issuer: `${API_URI}/${issuer}`,
    jwtid: token_id,
    algorithm: "HS256",
    audience: AUDIENCE
  })
}