module.exports = (res, error, status) => {
  res.status(status).json({ error })
}
