const { verify } = require("jsonwebtoken")

const SIGNING_KEY = process.env.JWT_SIGNING_KEY

module.exports = (jwt) => {
  try {
    const decoded = verify(jwt, SIGNING_KEY, { algorithms: "HS256" })
    return decoded
  } catch(e) {
    return null
  }
}