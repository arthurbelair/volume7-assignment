const handleError = require("./lib/handleError")

const Note = require("../models").Note

module.exports = async (req, res) => {
  try {
    const { userInfo } = res.locals
    const { content } = req.body
  
    const note = await Note.create({
      content,
      userId: userInfo.sub
    })

    res.status(201).json({
      note
    })
  } catch(e) {
    handleError(res, e.message, 500)
  }
}