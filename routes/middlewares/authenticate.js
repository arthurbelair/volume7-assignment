const handleError = require("../lib/handleError")
const verifyJWT = require("../lib/verifyJWT")

module.exports = (req, res, next) => {
  const token = req.get("authorization")

  if (!token) return handleError(res, "User must be logged in.", 403)

  const userInfo = verifyJWT(token)
  if (!userInfo) return handleError(res, "User must be logged in.", 403)

  res.locals.userInfo = userInfo
  next()
}