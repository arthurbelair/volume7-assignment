const { validationResult } = require("express-validator")

module.exports = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const arr = errors.array()
    const lastError = arr[arr.length - 1]
    return res.status(400).json({ error: lastError.msg })
  }

  next()
}