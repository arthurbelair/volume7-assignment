const User = require("../models").User
const bcrypt = require("bcrypt")
const generateJWT = require("./lib/generateJWT")
const handleError = require("./lib/handleError")

const SALT_ROUNDS = 10

module.exports = async (req, res) => {
  const { email, password } = req.body

  try {
    const password_hash = bcrypt.hashSync(password, SALT_ROUNDS)
    const user = await User.create({ email, password_hash })

    const token = generateJWT(user, { issuer: "api/signup" })

    res.status(201).json({ token })
  } catch(e) {
    if (e.name === "SequelizeUniqueConstraintError") {
      handleError(res, "User already exists.", 409)
    } else {
      handleError(res, e.message, 500)
    }
  }
}