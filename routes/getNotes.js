const Note = require("../models").Note

module.exports = async (req, res) => {
  const { userInfo } = res.locals

  const notes = await Note.findAll({
    where: { userId: userInfo.sub }
  })

  res.json({
    notes
  })
}